import React from 'react';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import {AppearanceProvider} from 'react-native-appearance';

import {Main} from './src/utils/navigation/main';
import SplashScreen from './src/screens/Splash';
import ErrorHandler from './src/components/ErrorHandler';

export default function App() {
  return (
    <SafeAreaProvider>
      <AppearanceProvider>
        <Main />
        <ErrorHandler />
      </AppearanceProvider>
    </SafeAreaProvider>
  );
}
