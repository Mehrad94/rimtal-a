//what is git
// version controll system
// created for linux kernal project
//created by linus Torvalds for kernal project
//local oprations
//Super Fast
//Open Sourse
//Huge community

//Reository
//History
//collection of files managed by Git
//save all history in .git Folder in local

//Commits
//any File Changes in Sourse means Commit
//The first brench name is master

//Github
//online repository System
//Free for unlimited public repos

//FIRST WE Have TO DO
//install git for OP
//git-scm.com
//git init in root project

//how to work
// Working directory  => staging area => repository(.git folder)  ===if remote repository exist next stage is =>remote repository

//FIRST COMMIT
//for show commit status ENTER [git status] command(this will show you branches and your commit name if exist)
//for adding a file in git repo [git add <filename> ] or [git add all ] for all files exist in project root (git add == stage files)
//finally for commit our changes in brench [git commit - m "first file in demo repo"]

//EXTENTION
//[rm -rf .git]  for remove git folder
//[git init .]   means make git foldet in this root or  folder
//[git add .] for all files stage in repo
// [git log] for full details of brench
// [git show ] show last commit and chanages
// press Q for closing
// [git ls-file] show all file axist in git
// [git commit - a] for first stage modified files and the commit
// [git commit -am "message" ] for stage and combine chnage

//CONCLUSION
//for test
// change a file content
//git starts
//git add . for stage
//git status  for showing changes

//UNSTAGE  CHANGES
//[git reset HEAD <filename>] unstage last change
//[git checkout -- <file name>] for disable changes

//EXTENTION
// [git log --oneline --graph --decorate --all]  for all history of git folder
// for customization and cngfig  shorter command [git config --global alias.hist "log --oneline --graph --decorate --all"]
// and git config --global --list  and [git hist]
// for find information about a file [git hist  -- <file name>]
// for make a file with git [touch <file name>.<file pasvand>]

//REMOVE AND RENAME FILEES WITH GIT
//[git mv <exist file name>.(js for example) <new file name>.(js for example)] for renaming file name
//[git rm <file name >] to remove file
//EXAMPLE
//[ls] => [git rm <file name> ] => [git status]  => [git commit -m "delete file"]
// for see deleted files [git add -u]
//for commit multiple action like delete and rename at a moment [git add -A]

//ADVANCE OPTIONS
//GAOLS
// COMPARE DIFRENCES
//BRANCHING MERGING CONFLICT RESOLUSTION and etc

//COMPARE DIFRENCES
//[git hist] for see commit history  and [git diff "commit id" HEAD(last commit)] for compare diffrences between commits
//we can also open merge tools (we must config it before) with [git difftool "commit id" HEAD]
// we can run also merge tool [git difftool]
// many diffrent options in [git help diff]
//============FEATURE BRANCH ====
//BRANCHING AND MERGING         ||                              ||
//========MASTER BRENCH =====  =========================================>   {automutic merge || manual merge }

//EXAMPLE
//[git branch] to see branch name
// [git checkout - b updates] to switch a new brench
//this is a technique you can use when you start working on master and you decide later that ,before
//committing that these changes really should be isolated into their own feature ot topic branch
// [git checkout master] for switch to master
// fast forward :merge master with another exist branch by [git merge updates]
// for disable this merge [git branch -d updates] and [git branch] to see master as branch again
