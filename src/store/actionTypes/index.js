import atRedux from './redux';
import atSaga from './saga';
const actionTypes = {
  atRedux,
  atSaga,
};
export default actionTypes;
