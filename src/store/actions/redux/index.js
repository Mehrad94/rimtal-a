import atRedux from '../../actionTypes/redux';

export const setSplashData = (data) => {
  return {
    type: atRedux.SET_SPLASH_DATA,
    payload: data,
  };
};
export const setError = (error) => {
  return {
    type: atRedux.SET_ERROR,
    payload: error,
  };
};
