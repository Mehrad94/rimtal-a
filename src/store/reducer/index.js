import {combineReducers} from 'redux';
import errorReducer from './erroeReducer';
import splashReducer from './splash';

const rootReducer = combineReducers({
  error: errorReducer,
  splash: splashReducer,
});

export default rootReducer;
