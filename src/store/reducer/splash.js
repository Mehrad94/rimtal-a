import atRedux from '../actionTypes/redux';

const initialState = {splashData: null};
const setSplashData = (state, action) => {
  return {...state, splashData: action.payload};
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case atRedux.SET_SPLASH_DATA:
      return setSplashData(state, action);
    default:
      return state;
  }
};

export default reducer;
