import atRedux from '../actionTypes/redux';

const initialState = {error: null};
const setError = (state, action) => {
  return {...state, error: action.payload};
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case atRedux.SET_ERROR:
      return setError(state, action);
    default:
      return state;
  }
};

export default reducer;
