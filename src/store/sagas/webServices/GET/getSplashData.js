import {delay} from 'redux-saga';
import {put, call} from 'redux-saga/effects';
import actions from '../../../actions';
import get from '../../../../values/api/routes/GET';
export function* getSplashDataSaga(action) {
  console.log({action});

  const delay = (ms) => new Promise((res) => setTimeout(res, ms));
  yield delay(100);

  const res = yield get.getSplash();
  console.log({res});

  if (res.data) {
    yield put(actions.reduxActions.setSplashData(res.data));
    yield delay(1000);
    yield action.callBack();
  } else {
    if (!res.error.response.CODE)
      yield put(actions.reduxActions.setError(1090));
    yield put(actions.reduxActions.setError(res.error.response.CODE));
  }
}
