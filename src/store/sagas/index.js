import {takeEvery, all, takeLatest} from 'redux-saga/effects';
import atSaga from '../actionTypes/saga';
// import {getSplashDataSaga} from './splash';
import webServices from './webServices';
export function* watchSplash() {
  yield takeEvery(
    atSaga.GET_INITIATE_SPLASH_DATA,
    webServices.GET.getSplashDataSaga,
  );
}
