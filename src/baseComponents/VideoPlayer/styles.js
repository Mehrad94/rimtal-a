import {StyleSheet, Dimensions} from 'react-native';
// import {GOLDEN_COLOR, MAIN_COLOR} from '../../common/Colors';
// import {iranSans} from '../../common/Theme';
// import {screenHeight} from '../../common/Constants';
const {width, height} = Dimensions.get('screen');
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#e8e8e8',
  },
  video: {
    height: width * (9 / 14),
    width: width,
  },
  fullscreenVideo: {
    height: width,
    width: height,
    backgroundColor: '#e8e8e8',
  },
  text: {
    marginTop: 10,
    marginHorizontal: 20,
    fontSize: 15,
    textAlign: 'right',
  },
  fullscreenButton: {
    flex: 1,
    flexDirection: 'row',
    alignSelf: 'flex-end',
    alignItems: 'center',
    paddingRight: 10,
  },
  controlOverlay: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    backgroundColor: '#000000c4',
    justifyContent: 'space-between',
  },
  title: {
    textAlign: 'right',
    fontSize: 18,
    color: 'red',
    borderRadius: 5,
  },
  scroll: {},
  likeHolder: {
    backgroundColor: 'red',
    paddingRight: 15,
    height: height / 13,
  },
  date: {
    color: 'red',
    textAlign: 'right',

    fontSize: 12,
  },
  box: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    backgroundColor: 'red',
    height: height / 13,
  },
  view: {
    marginLeft: 5,
    justifyContent: 'center',
    alignSelf: 'center',
    width: width / 3.5,
  },
  comment: {
    alignSelf: 'center',
  },
  textComment: {
    color: 'red',
  },
  icon: {
    alignSelf: 'center',
  },
});

export default styles;
