import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import Icon from 'react-native-vector-icons/Feather';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'red',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 16,
    marginBottom: 18,
  },
  text: {
    color: 'white',
  },
});

const MiniPlayer = () => {
  return (
    <View style={styles.container}>
      <Icon name="heart" color="white" size={24} />
      <Text style={styles.text}>Metronomy - The Bay</Text>
      <Icon name="play-circle" color="white" size={24} />
    </View>
  );
};
export default MiniPlayer;
