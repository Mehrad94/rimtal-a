import React from 'react';
import {SafeAreaView, ScrollView, Text, View, TouchableOpacity} from 'react-native';
import {useTheme} from 'react-native-paper';
import CustomIcon from "Rimtal/src//utils/CustomIcon";
import styles from './styles';
import str from 'Rimtal/src/values/strings';
const profile = () => {
  const theme = useTheme();
  return (
    <SafeAreaView style={styles.container} >
    <ScrollView scrollEventThrottle={1} showsVerticalScrollIndicator={false}> 

    <TouchableOpacity style={styles.touch}>
     <View style={styles.view}>
         <CustomIcon name="user-edit" color={"#888888"} size={20} style={styles.icon} />
         <Text style={[theme.titleStyle,{marginLeft:15}]}>{str.SHUFFLE_MY_MUSIC}</Text>
     </View>
     <CustomIcon name="chevron-right" color={"#888888"} size={20} style={styles.icon2} />
    </TouchableOpacity>

    <TouchableOpacity style={styles.touch}>
     <View style={styles.view}>
         <CustomIcon name="outdent" color={"#888888"} size={20} style={styles.icon} />
         <Text style={theme.titleStyle}>{str.DOWNLOADED}</Text>
     </View>
     <CustomIcon name="chevron-right" color={"#888888"} size={20} style={styles.icon2} />
    </TouchableOpacity>

    <TouchableOpacity style={styles.touch}>
     <View style={styles.view}>
         <CustomIcon name="moon" color={"#888888"} size={20} style={styles.icon} />
         <Text style={theme.titleStyle}>{str.PLAYLISTS}</Text>
     </View>
     <CustomIcon name="chevron-right" color={"#888888"} size={20} style={styles.icon2} />
    </TouchableOpacity>
       
    <TouchableOpacity style={styles.touch}>
     <View style={styles.view}>
         <CustomIcon name="plane" color={"#888888"} size={20} style={styles.icon} />
         <Text style={theme.titleStyle}>{str.ALBUMS}</Text>
     </View>
     <CustomIcon name="chevron-right" color={"#888888"} size={20} style={styles.icon2} />
    </TouchableOpacity>

    <TouchableOpacity style={styles.touch}>
     <View style={styles.view}>
         <CustomIcon name="comments" color={"#888888"} size={20} style={styles.icon} />
         <Text style={[theme.titleStyle,{marginLeft:17}]}>{str.TRACKS}</Text>
     </View>
     <CustomIcon name="chevron-right" color={"#888888"} size={20} style={styles.icon2} />
    </TouchableOpacity>

    
    </ScrollView>
    </SafeAreaView>
  );
};

export default profile;