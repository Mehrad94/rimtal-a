import {StyleSheet, Dimensions} from 'react-native';
const {width} = Dimensions.get('screen');

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop:10
  },
  touch: {
   flexDirection:'row',
   justifyContent:'space-between',
   marginHorizontal:20,
   marginVertical:10
  },
  view: {
    flexDirection:'row', 
  },
  icon: {
      
      alignSelf:'center'
  },
  icon2: {
    alignSelf:'center'
}
});

export default styles;