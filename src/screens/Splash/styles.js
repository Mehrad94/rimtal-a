import {StyleSheet, Dimensions} from 'react-native';
const {width, height} = Dimensions.get('screen');
export default styles = StyleSheet.create({
  container: {flex: 1, justifyContent: 'center', alignItems: 'center'},
  logo: {width: width / 3, height: width / 3},
});
