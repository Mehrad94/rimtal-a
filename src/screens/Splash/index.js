import React, {useEffect} from 'react';
import {View, Text, Image} from 'react-native';
import Containers from 'Rimtal/src/utils/containers';
import {useDispatch, useSelector} from 'react-redux';
import * as sagaActions from '../../store/actions/saga';
import styles from './styles';
import {useTheme} from 'react-native-paper';
import OfflineVideoSection from '../../baseComponents/VideoPlayer';
const Logo = require('../../assets/images/png/lg_rimtal_w.png');
const SplashScreen = ({navigation}) => {
  const theme = useTheme();
  const {primary} = theme.customColors;
  // console.log({theme});
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(
      sagaActions.getSpalashData(() => {
        navigation.navigate('Botton');
      }),
    );
  }, []);

  return (
    <View style={[styles.container, {backgroundColor: primary}]}>
      {/* <OfflineVideoSection /> */}
      <Image source={Logo} style={styles.logo} />
    </View>
  );
};
export default SplashScreen;
