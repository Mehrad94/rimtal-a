import React, {useEffect} from 'react';
import {View, FlatList} from 'react-native';
import SliderCard from '../../../components/cards/SliderCard';
import {useSelector} from 'react-redux';
const SliderList = ({theme}) => {
  const sliders = useSelector((state) => {
    return state.splash.splashData.sliders;
  });
  const renderItems = ({item}) => {
    return <SliderCard items={item} theme={theme} />;
  };
  const keyExtractor = (item, index) => {
    return item._id;
  };
  const getItemLayout = (data, index) => ({
    length: 100,
    offset: 100 * index,
    index,
  });
  return (
    <View>
      <FlatList
        showsHorizontalScrollIndicator={false}
        horizontal
        data={sliders}
        renderItem={renderItems}
        keyExtractor={keyExtractor}
        removeClippedSubviews={true}
        initialNumToRender={5}
        maxToRenderPerBatch={1}
        windowSize={7}
        getItemLayout={getItemLayout}
      />
    </View>
  );
};
export default SliderList;
