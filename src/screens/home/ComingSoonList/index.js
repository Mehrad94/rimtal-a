import React, {useEffect} from 'react';
import {View, FlatList, Text} from 'react-native';
import styles from './styles';
import PlayListCard from '../../../components/cards/PlayListCard';
import str from 'Rimtal/src/values/strings';
import {useSelector} from 'react-redux';
import ComingSoonCard from '../../../components/cards/ComminSoonCard';

const ComingSoonList = ({theme}) => {
  const coming = useSelector((state) => {
    return state.splash.splashData.comingSoon;
  });
  const renderItems = ({item}) => {
    return <ComingSoonCard items={item} theme={theme} />;
  };
  const keyExtractor = (item, index) => {
    return item._id;
  };
  const getItemLayout = (data, index) => ({
    length: 100,
    offset: 100 * index,
    index,
  });
  return (
    <View>
      <View style={styles.textView}>
        <Text style={theme.titleStyle}>{str.COMING_SOON}</Text>
        <Text style={theme.optTitleStyle}>{str.SEE_ALL}</Text>
      </View>
      <FlatList
        showsHorizontalScrollIndicator={false}
        horizontal
        data={coming}
        renderItem={renderItems}
        keyExtractor={keyExtractor}
        removeClippedSubviews={true}
        initialNumToRender={5}
        maxToRenderPerBatch={1}
        // windowSize={7}
        getItemLayout={getItemLayout}
      />
    </View>
  );
};
export default ComingSoonList;
