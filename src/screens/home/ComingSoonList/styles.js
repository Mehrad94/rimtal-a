import {StyleSheet, Dimensions} from 'react-native';
const {width} = Dimensions.get('screen');

const styles = StyleSheet.create({
 
  textView: {
    flexDirection: 'row',
    justifyContent: 'space-between',
   marginEnd:16
  },
  moreText: {
    marginRight: 16,
    marginVertical: 5,
  },
});

export default styles;
