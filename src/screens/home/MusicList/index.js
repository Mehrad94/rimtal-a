import React, {useEffect} from 'react';
import {View, FlatList, Text} from 'react-native';
import AlbumsCard from '../../../components/cards/AlbumsCard';
import styles from './styles';
import str from 'Rimtal/src/values/strings';
import {useSelector} from 'react-redux';

const MusicList = ({theme}) => {
  const subTitle = theme.optTitleStyle;
  const title = theme.titleStyle;
  const albums = useSelector((state) => {
    return state.splash.splashData.albums;
  });

  const renderItems = (items) => {
    return <AlbumsCard items={items.item} theme={theme} />;
  };
  const keyExtractor = (item, index) => {
    return item[0]._id;
  };
  const getItemLayout = (data, index) => ({
    length: 100,
    offset: 100 * index,
    index,
  });

  return (
    <View>
      <View style={styles.textView}>
        <Text style={[title]}>{str.ALBUMS_TITLE}</Text>
        <Text style={[subTitle]}>{str.SEE_ALL}</Text>
      </View>
      <FlatList
        showsHorizontalScrollIndicator={false}
        horizontal
        data={albums}
        renderItem={renderItems}
        keyExtractor={keyExtractor}
        removeClippedSubviews={true}
        initialNumToRender={5}
        maxToRenderPerBatch={1}
        // windowSize={7}
        getItemLayout={getItemLayout}
      />
    </View>
  );
};
export default MusicList;
