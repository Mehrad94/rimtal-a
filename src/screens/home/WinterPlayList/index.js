import React, {useEffect} from 'react';
import {View, FlatList, Text} from 'react-native';
import styles from './styles';
import PlayListCard from '../../../components/cards/PlayListCard';
import str from 'Rimtal/src/values/strings';
const WinterPlayList = ({theme}) => {
  const title = 'WinterPlayList';
  const category = [
    {
      newAlbum: 'New Album',
      albumName: 'King',
      artistName: 'Jenifer',
      img: require('../../../assets/images/jpeg/2.jpg'),
    },
    {
      newAlbum: 'New Album',
      albumName: 'morteza',
      artistName: 'Morteza',
      img: require('../../../assets/images/jpeg/2.jpg'),
    },
    {
      newAlbum: 'New Album',
      albumName: 'King',
      artistName: 'Jenifer',
      img: require('../../../assets/images/jpeg/2.jpg'),
    },
    {
      newAlbum: 'New Album',
      albumName: 'morteza',
      artistName: 'Morteza',
      img: require('../../../assets/images/jpeg/2.jpg'),
    },
    {
      newAlbum: 'New Album',
      albumName: 'King',
      artistName: 'Jenifer',
      img: require('../../../assets/images/jpeg/5.jpg'),
    },
  ];
  const renderItems = ({item}) => {
    return <PlayListCard items={item} theme={theme} />;
  };
  return (
    <View>
      <View style={styles.textView}>
        <Text style={theme.titleStyle}>{title}</Text>
        <Text style={theme.optTitleStyle}>{str.SEE_ALL}</Text>
      </View>
      <FlatList
        showsHorizontalScrollIndicator={false}
        horizontal
        data={category}
        renderItem={renderItems}
      />
    </View>
  );
};
export default WinterPlayList;
