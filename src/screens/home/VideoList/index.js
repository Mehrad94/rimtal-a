import React, {useEffect} from 'react';
import {View, FlatList, Text} from 'react-native';
import VideoCard from '../../../components/cards/VideoCard';
import styles from './styles';
import {useSelector} from 'react-redux';
import str from 'Rimtal/src/values/strings';

const VideoList = ({theme}) => {
  const videos = useSelector((state) => {
    return state.splash.splashData.musicVideos;
  });

  const renderItems = ({item}) => {
    return <VideoCard items={item} theme={theme} />;
  };
  const keyExtractor = (item, index) => {
    return item._id;
  };
  const getItemLayout = (data, index) => ({
    length: 100,
    offset: 100 * index,
    index,
  });
  return (
    <View>
      <View style={styles.textView}>
        <Text style={theme.titleStyle}>{str.MUSIC_VIDEO}</Text>
        <Text style={theme.optTitleStyle}>{str.SEE_ALL}</Text>
      </View>
      <FlatList
        showsHorizontalScrollIndicator={false}
        horizontal
        data={videos}
        renderItem={renderItems}
        keyExtractor={keyExtractor}
        removeClippedSubviews={true}
        initialNumToRender={5}
        maxToRenderPerBatch={1}
        // windowSize={1}
        getItemLayout={getItemLayout}
      />
    </View>
  );
};
export default VideoList;
