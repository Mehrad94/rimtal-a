import React from 'react';
import {SafeAreaView, ScrollView} from 'react-native';
// import styles from './styles';
import SliderList from './SliderList';
import VideoList from './VideoList';
import MusicList from './MusicList';
import SingleList from './SingleList';
import PlayList from './PlayList';
import WinterPlayList from './WinterPlayList';
import ComingSoonList from './ComingSoonList';
import {useTheme} from 'react-native-paper';

const home = () => {
  const theme = useTheme();
  return (
    <SafeAreaView style={{marginStart: 16}}>
      <ScrollView scrollEventThrottle={1} showsVerticalScrollIndicator={false}>
        <SliderList theme={theme} />
        <MusicList theme={theme} />
        <SingleList theme={theme} />
        <PlayList theme={theme} />
        <VideoList theme={theme} />
        <ComingSoonList theme={theme} />
      </ScrollView>
    </SafeAreaView>
  );
};

export default home;

{
  /* <WinterPlayList theme={theme} /> */
}
