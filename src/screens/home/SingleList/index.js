import React, {useEffect} from 'react';
import {View, FlatList, Text} from 'react-native';
import SingleCard from '../../../components/cards/SingleCard';
import styles from './styles';
import str from 'Rimtal/src/values/strings';

import {useSelector} from 'react-redux';

const SingleList = ({theme}) => {
  const singles = useSelector((state) => {
    return state.splash.splashData.singles;
  });

  const renderItems = ({item}) => {
    return <SingleCard items={item} theme={theme} />;
  };
  const keyExtractor = (item, index) => {
    return item[0]._id;
  };
  const getItemLayout = (data, index) => ({
    length: 100,
    offset: 100 * index,
    index,
  });
  return (
    <View>
      <View style={styles.textView}>
        <Text style={theme.titleStyle}>{str.SINGLE_TITLE}</Text>
        <Text style={theme.optTitleStyle}>{str.SEE_ALL}</Text>
      </View>
      <FlatList
        showsHorizontalScrollIndicator={false}
        horizontal
        data={singles}
        renderItem={renderItems}
        keyExtractor={keyExtractor}
        removeClippedSubviews={true}
        initialNumToRender={5}
        maxToRenderPerBatch={1}
        // windowSize={7}
        getItemLayout={getItemLayout}
      />
    </View>
  );
};
export default SingleList;
