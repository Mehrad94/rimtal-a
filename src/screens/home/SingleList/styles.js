import {StyleSheet, Dimensions} from 'react-native';
const {width} = Dimensions.get('screen');

const styles = StyleSheet.create({
  text: {
    marginRight: 16,
    marginEnd:16
  },
  textView: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginEnd:16,
  },
  moreText: {
    marginRight: 16,
    marginVertical: 5,
  },
});

export default styles;
