import React from 'react';
import {SafeAreaView, ScrollView, Text, View} from 'react-native';
import {useTheme} from 'react-native-paper';
import NewRealesForYou from './NewRealesForYou';
import PlaylistForYou from './PlaylistForYou';
import VideoForYou from './VideoForYou';
import ArtistForYou from './ArtistForYou';
const discover = () => {
  const theme = useTheme();
  return (
    <SafeAreaView style={{marginStart: 16}}>
    <ScrollView scrollEventThrottle={1} showsVerticalScrollIndicator={false}> 
        <NewRealesForYou theme={theme} />
        <ArtistForYou theme={theme} />
        <PlaylistForYou theme={theme}  />
        <VideoForYou theme={theme} />
    </ScrollView>
    </SafeAreaView>
  );
};

export default discover;