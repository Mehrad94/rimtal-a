import {StyleSheet, Dimensions} from 'react-native';
const {width} = Dimensions.get('screen');

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginStart:20,
    marginTop:9
  },
  containers:{
marginBottom:37
  },
  touch: {
   flexDirection:'row',
   justifyContent: 'space-between',
   marginEnd:20,
   marginVertical:11
  },
  view: {
    flexDirection:'row', 
  },
  icon: {
      
      alignSelf:'center'
  },
  icon2: {
    alignSelf:'center'
},
text:{
  marginStart:17
}
});

export default styles;