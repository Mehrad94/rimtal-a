import React from 'react';
import {SafeAreaView, View, Text, TouchableOpacity} from 'react-native';
import {useTheme} from 'react-native-paper';
import CustomIcon from "Rimtal/src//utils/CustomIcon";
import styles from './styles';
import str from 'Rimtal/src/values/strings';
import RecentlyPlayed from './RecentlyPlayed';
const myMusic = () => {
  const theme = useTheme();
  return (
    <SafeAreaView style={styles.container} >
    <View style={styles.containers}> 

    <TouchableOpacity style={styles.touch}>
     <View style={styles.view}>
         <CustomIcon name="random" color={"#888888"} size={20} style={styles.icon} />
         <Text style={[theme.optStyle,styles.text]}>{str.SHUFFLE_MY_MUSIC}</Text>
     </View>
     <CustomIcon name="chevron-right" color={"#888888"} size={20} style={styles.icon2} />
    </TouchableOpacity>

    <TouchableOpacity style={styles.touch}>
     <View style={styles.view}>
         <CustomIcon name="arrow-circle-down" color={"#888888"} size={20} style={styles.icon} />
         <Text style={[theme.optStyle,styles.text]}>{str.DOWNLOADED}</Text>
     </View>
     <CustomIcon name="chevron-right" color={"#888888"} size={20} style={styles.icon2} />
    </TouchableOpacity>

    <TouchableOpacity style={styles.touch}>
     <View style={styles.view}>
         <CustomIcon name="list-music" color={"#888888"} size={20} style={styles.icon} />
         <Text style={[theme.optStyle,styles.text]}>{str.PLAYLISTS}</Text>
     </View>
     <CustomIcon name="chevron-right" color={"#888888"} size={20} style={styles.icon2} />
    </TouchableOpacity>
       
    <TouchableOpacity style={styles.touch}>
     <View style={styles.view}>
         <CustomIcon name="album" color={"#888888"} size={20} style={styles.icon} />
         <Text style={[theme.optStyle,styles.text]}>{str.ALBUMS}</Text>
     </View>
     <CustomIcon name="chevron-right" color={"#888888"} size={20} style={styles.icon2} />
    </TouchableOpacity>

    <TouchableOpacity style={styles.touch}>
     <View style={styles.view}>
         <CustomIcon name="music" color={"#888888"} size={20} style={styles.icon} />
         <Text style={[theme.optStyle,styles.text]}>{str.TRACKS}</Text>
     </View>
     <CustomIcon name="chevron-right" color={"#888888"} size={20} style={styles.icon2} />
    </TouchableOpacity>

    <TouchableOpacity style={styles.touch}>
     <View style={styles.view}>
         <CustomIcon name="tv-music" color={"#888888"} size={20} style={styles.icon} />
         <Text style={[theme.optStyle,styles.text]}>{str.VIDEOS}</Text>
     </View>
     <CustomIcon name="chevron-right" color={"#888888"} size={20} style={styles.icon2} />
    </TouchableOpacity>

    <TouchableOpacity style={styles.touch}>
     <View style={styles.view}>
         <CustomIcon name="user-music" color={"#888888"} size={20} style={styles.icon} />
         <Text style={[theme.optStyle,styles.text]}>{str.ARTISTS}</Text>
     </View>
     <CustomIcon name="chevron-right" color={"#888888"} size={20} style={styles.icon2} />
    </TouchableOpacity>
  
    </View>
    <RecentlyPlayed theme={theme} />
    </SafeAreaView>
  );
};

export default myMusic;