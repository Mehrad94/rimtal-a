import React, {useEffect} from 'react';
import {View, FlatList, Text} from 'react-native';
import styles from './styles';
import PlayListCard from '../../../components/cards/PlayListCard';
import {useSelector} from 'react-redux';
import str from 'Rimtal/src/values/strings';

const RecentlyPlayed = ({theme}) => {
  const playList = useSelector((state) => {
    return state.splash.splashData.playlists;
  });

  const renderItems = ({item}) => {
    return <PlayListCard items={item} theme={theme} />;
  };
  const keyExtractor = (item, index) => {
    return item._id;
  };
  const getItemLayout = (data, index) => ({
    length: 100,
    offset: 100 * index,
    index,
  });

  
  return (
    <View>
      <View style={styles.textView}>
        <Text style={theme.titleStyle}>{str.RECENTLY_PLAYED}</Text>
        <Text style={theme.optTitleStyle}>{str.SEE_ALL}</Text>
      </View>
      <FlatList
        showsHorizontalScrollIndicator={false}
        horizontal
        data={playList}
        renderItem={renderItems}
        keyExtractor={keyExtractor}
        removeClippedSubviews={true}
        initialNumToRender={5}
        maxToRenderPerBatch={1}
        // windowSize={7}
        getItemLayout={getItemLayout}
      />
    </View>
  );
};
export default RecentlyPlayed;
