const dark = {
  customColors: {primary: '#0070ef'},
  titleStyle: {color: 'white', fontSize: 18},
  optTitleStyle: {color: 'white', fontSize: 16},
};
export default dark;
