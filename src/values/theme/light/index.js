import utils from '../../../utils';
const font = utils.fontChanger();
const light = {
  customColors: {primary: '#0070ef'},
  titleStyle: [
    {
      color: '#666666',
      fontSize: 18,
      fontWeight: 'bold',
      fontFamily: 'Mabry Pro Black',
    },
  ],
  optStyle: [
    {color: '#666666', fontSize: 18, fontFamily: 'SF-Pro-Display-Regular'},
  ],
  optTitleStyle: [
    {color: '#999999', fontSize: 16, fontFamily: 'SF-Pro-Display-Regular'},
  ],
  name: [
    {color: '#666666', fontSize: 16, fontFamily: 'SF-Pro-Display-Regular'},
  ],
  artistName: [
    {
      color: '#666666',
      fontSize: 14,
      lineHeight: 20,
      fontFamily: 'SF-Pro-Display-Regular',
    },
  ],
  red: [{color: '#DB4646', fontSize: 16, fontFamily: 'SF-Pro-Display-Regular'}],
};
export default light;
