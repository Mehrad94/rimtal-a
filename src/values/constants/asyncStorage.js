const LANGUAGE = 'language';
const THEME = 'theme';
const asyncStorage = {
  LANGUAGE,
  THEME,
};
export default asyncStorage;
