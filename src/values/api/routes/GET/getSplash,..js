import axios from '../../AxiosConfigue';
import {I18nManager} from 'react-native';
const url = I18nManager.isRTL ? '/splashScreen/fa' : '/splashScreen';
export const getSplash = async () =>
  axios
    .get(url)
    .then((res) => {
      return {data: res.data};
    })
    .catch((e) => {
      console.log({e});

      return {error: e};
    });
