import get from './GET';

const routes = {
  get,
};

export default routes;
