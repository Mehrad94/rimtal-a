const APP_NAME = 'ریمتال';
const ALBUMS_TITLE = 'آلبوم ها';
const SINGLE_TITLE = 'تک اهنگ ها';
const SEE_ALL = 'دیدن همه';
const PLAYLIST_TITLE = 'لیست پخش';
const COMING_SOON = 'به زودی';
const NEW_REALLES_FOR_YOU = 'جدیدترین ها  برای شما';
const PLAYLIST_FOR_YOU = 'لیست پخش برای شما';
const VIDEO_FOR_YOU = 'ویدئو های شما';
const ARTIST_FOR_YOU = 'خواننده های شما';
const SHUFFLE_MY_MUSIC = 'موزیک رندم من';
const DOWNLOADED = 'دانلود شده ها';
const PLAYLISTS = 'لیست پخش ها';
const ALBUMS = 'آلبوم ها';
const TRACKS = 'آهنگ ها';
const VIDEOS = 'ویدئو ها';
const ARTISTS = 'خواننده ها';
const MUSIC_VIDEO = 'موزیک ویدیو';
const RECENTLY_PLAYED = 'پخش های اخیر'

const fa = {
  APP_NAME,
  ALBUMS_TITLE,
  SINGLE_TITLE,
  SEE_ALL,
  PLAYLIST_TITLE,
  COMING_SOON,
  NEW_REALLES_FOR_YOU,
  PLAYLIST_FOR_YOU,
  VIDEO_FOR_YOU,
  ARTIST_FOR_YOU,
  SHUFFLE_MY_MUSIC,
  DOWNLOADED,
  PLAYLISTS,
  ALBUMS,
  TRACKS,
  VIDEOS,
  ARTISTS,
  MUSIC_VIDEO,
  RECENTLY_PLAYED
};
export default fa;
