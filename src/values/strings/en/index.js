const APP_NAME = 'Rimtal';
const ALBUMS_TITLE = 'Albums';
const SINGLE_TITLE = 'Single';
const SEE_ALL = 'see all';
const PLAYLIST_TITLE = 'PlayList';
const COMING_SOON = 'Coming Soon';
const MUSIC_VIDEO = 'MusicVideo';
const NEW_REALLES_FOR_YOU = 'New Reales For You';
const PLAYLIST_FOR_YOU = 'Playlist For You';
const VIDEO_FOR_YOU = 'Video For You';
const ARTIST_FOR_YOU = 'Artist For You';
const SHUFFLE_MY_MUSIC = 'shuffle My Music';
const DOWNLOADED = 'Downloaded';
const PLAYLISTS = 'Playlists';
const ALBUMS = 'Albums';
const TRACKS = 'Tracks';
const VIDEOS = 'Videos';
const ARTISTS = 'Artists';
const RECENTLY_PLAYED = 'Recently Played'

const en = {
  APP_NAME,
  ALBUMS_TITLE,
  SINGLE_TITLE,
  SEE_ALL,
  PLAYLIST_TITLE,
  COMING_SOON,
  MUSIC_VIDEO,
  NEW_REALLES_FOR_YOU,
  PLAYLIST_FOR_YOU,
  VIDEO_FOR_YOU,
  ARTIST_FOR_YOU,
  SHUFFLE_MY_MUSIC,
  DOWNLOADED,
  PLAYLISTS,
  ALBUMS,
  TRACKS,
  VIDEOS,
  ARTISTS,
  RECENTLY_PLAYED
};
export default en;
