import {I18nManager} from 'react-native';
import en from './en';
import fa from './fa';
const direction = I18nManager.isRTL;

let str;
if (direction === true) str = fa;
else str = en;

export default str;
