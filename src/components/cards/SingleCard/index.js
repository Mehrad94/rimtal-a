import React from 'react';
import {Image, Text, TouchableOpacity, View} from 'react-native';
import styles from './styles';
import util from 'Rimtal/src/utils';

const SingleCard = ({items, theme}) => {
  let firstItem = items[0] ? items[0] : null;
  let secondItem = items[1] ? items[1] : null;
  const title = firstItem ? firstItem.title : '';
  const titleSec = secondItem ? secondItem.title : '';
  const artistName = firstItem ? firstItem.artist : '';
  const artistNameSec = secondItem ? secondItem.artist : '';

  return (
    <View style={{flex: 1}}>
      {firstItem ? (
        <TouchableOpacity activeOpacity={0.7} style={styles.container}>
          <Image style={styles.img} source={{uri: items[0].image}} />
          <Text style={[theme.name]} numberOfLines={1}>
            {title}
          </Text>
          <Text style={theme.artistName} numberOfLines={1}>
            {artistName}
          </Text>
        </TouchableOpacity>
      ) : null}
      {secondItem ? (
        <TouchableOpacity activeOpacity={0.7} style={styles.containers}>
          <Image style={styles.img} source={{uri: items[1].image}} />
          <Text style={[theme.name]} numberOfLines={1}>
            {titleSec}
          </Text>
          <Text style={theme.artistName} numberOfLines={1}>
            {artistNameSec}
          </Text>
        </TouchableOpacity>
      ) : null}
    </View>
  );
};

export default SingleCard;
