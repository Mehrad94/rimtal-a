import {StyleSheet, Dimensions} from 'react-native';
const {width} = Dimensions.get('screen');

const styles = StyleSheet.create({
  container: {
    marginTop: 16,
    width: width *.4,
    marginEnd:20
  },
  containers: {
    marginTop: 16,
    width: width *.4,
    marginBottom: 30,
    marginEnd:20
  },
  img: {
    width: width *.4,
    height: width *.4,
    borderRadius: 5,
  },
  text: {},
});

export default styles;
