import React from 'react';
import {Image, Text, TouchableOpacity, View} from 'react-native';
import styles from './styles';
import util from 'Rimtal/src/utils';

const AlbumsCard = ({items, theme}) => {
  let firstItem = items[0] ? items[0] : null;
  let secondItem = items[1] ? items[1] : null;
  const albumName = items[0] ? items[0].title : '';
  const albumNameSec = items[1] ? items[1].title : '';
  const artistName = items[0] ? items[0].artist : '';
  const artistNameSec = items[1] ? items[1].artist : '';

  return (
    <View style={{flex: 1}}>
      {firstItem ? (
        <TouchableOpacity activeOpacity={0.7} style={styles.container}>
          <Image style={styles.img} source={{uri: items[0].image}} />
          <Text style={[theme.name, styles.text]} numberOfLines={1}>
            {albumName}
          </Text>
          <Text style={[theme.artistName]}>{artistName}</Text>
        </TouchableOpacity>
      ) : null}
      {secondItem ? (
        <TouchableOpacity style={styles.containers}>
          <Image style={styles.img} source={{uri: items[1].image}} />
          <Text style={[theme.name, styles.text]} numberOfLines={1}>
            {albumNameSec}
          </Text>
          <Text style={[theme.artistName]}>{artistNameSec}</Text>
        </TouchableOpacity>
      ) : null}
    </View>
  );
};

export default AlbumsCard;
