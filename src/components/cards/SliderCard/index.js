import React from 'react';
import {Image, Text, TouchableOpacity, View} from 'react-native';
import styles from './styles';

const SliderCard = ({items, theme}) => {
  const sliderTitle = items.title ? items.title : '';
  const parentName = items.parentName ? items.parentName : '';
  const artistName = items.parentArtist ? items.parentArtist : '';

  return (
    <TouchableOpacity activeOpacity={0.7} style={styles.container}>
      <Text style={[theme.name, {marginBottom: 4}]} numberOfLines={1}>
        {sliderTitle}
      </Text>
      <Text style={[theme.red]} numberOfLines={1}>
        {parentName}
      </Text>
      <Text style={[theme.optStyle, {lineHeight: 21}]} numberOfLines={1}>
        {artistName}
      </Text>
      <Image style={styles.img} source={{uri: items.image}} />
    </TouchableOpacity>
  );
};

export default SliderCard;
