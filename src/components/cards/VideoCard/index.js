import React from 'react';
import {Image, Text, TouchableOpacity} from 'react-native';
import styles from './styles';
import img from '../../../assets/images/jpeg/1.jpg';
import util from 'Rimtal/src/utils';

const VideoCard = ({items, theme}) => {
  const videoName = items ? items.title : '';
  const artistName = items ? items.artist : '';
  const image = items ? items.thumbnail : '';
  return (
    <TouchableOpacity activeOpacity={0.7} style={styles.container}>
      <Image style={styles.img} source={{uri: image}} />
      <Text style={theme.name} numberOfLines={1}>{videoName}</Text>
      <Text style={theme.artistName} numberOfLines={1}>{artistName}</Text>
    </TouchableOpacity>
  );
};

export default VideoCard;
