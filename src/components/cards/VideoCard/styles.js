import { StyleSheet, Dimensions } from "react-native";
const {  width } = Dimensions.get("screen");

const styles = StyleSheet.create({
  container: {
    marginTop: 16,
    marginBottom: 30,
    marginEnd:20,
    width: width *.7,
  },
   img: {
       width: width *.7,
       height: width *.4,
   },
   text:{
    fontSize:16  
  },
  
});

export default styles;