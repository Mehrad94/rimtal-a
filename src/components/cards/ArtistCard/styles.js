import { StyleSheet, Dimensions } from "react-native";
const {  width } = Dimensions.get("screen");

const styles = StyleSheet.create({
  container: {
    marginTop: 16,
    marginBottom: 30,
    marginEnd:20,
    width: width *.4,
  },
   img: {
       width: width *.4,
       height: width *.4,
       borderRadius: width *.4/2,
   }
  
});

export default styles;
