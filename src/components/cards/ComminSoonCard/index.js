import React from 'react';
import {Image, Text, TouchableOpacity, View} from 'react-native';
import styles from './styles';
import util from 'Rimtal/src/utils';
const ComingSoonCard = ({items, theme}) => {
  const albumName = items ? items.title : '';
  const description = items ? items.artist : '';
  const image = items ? items.images : '';
  return (
    <TouchableOpacity activeOpacity={0.7} style={styles.container}>
      <Image style={styles.img} source={{uri: image}} />
      <Text style={[theme.name]} numberOfLines={1}>{albumName}</Text>
      <Text style={[theme.artistName]} numberOfLines={1}>{description}</Text>
    </TouchableOpacity>
  );
};

export default ComingSoonCard;
