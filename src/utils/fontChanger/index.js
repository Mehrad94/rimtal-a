import {I18nManager} from 'react-native';

export const fontChanger = () => {
  if (I18nManager.isRTL) {
    return {fontFamily: 'IRANYekanLight'};
  } else {
    return {fontFamily: 'IRANYekanLight'};
  }
};
