import {I18nManager} from 'react-native';
const translate = (obj, valueFa, valueEn) => {
  if (I18nManager.isRTL) {
    return obj[valueFa];
  } else {
    return obj[valueEn];
  }
};
export default translate;
