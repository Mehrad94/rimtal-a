import as from './asyncStorage/as';
import translate from './translator';
import {fontChanger} from './fontChanger';
const util = {
  as,
  translate,
  fontChanger,
};
export default util;
