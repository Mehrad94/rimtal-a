import React, {useState, useEffect} from 'react';
import RNRestart from 'react-native-restart';
import AsyncStorage from '@react-native-community/async-storage';
import dark from 'Rimtal/src/values/theme/dark';
import light from 'Rimtal/src/values/theme/light';
import {
  Provider as PaperProvider,
  DefaultTheme,
  DarkTheme,
} from 'react-native-paper';
import util from 'Rimtal/src/utils';
import {I18nManager} from 'react-native';
import {useColorScheme} from 'react-native-appearance';

import {RootNavigator} from '../rootNavigator';
import {PreferencesContext} from '../prefrencesContext';
import constans from 'Rimtal/src/values/constants';
import strings from 'Rimtal/src/values/strings';

export const Main = () => {
  // console.log({strings});

  const checkAsync = async () => {
    const key = constans.asyncStorage.THEME;
    const as = await AsyncStorage.getItem(key);
    setTheme(as);
  };
  // const colorScheme = useColorScheme();
  // console.log({colorScheme});

  const [theme, setTheme] = useState('light');
  const [rtl] = useState(I18nManager.isRTL);
  // console.log({theme});
  // console.log({util});
  useEffect(() => {
    checkAsync();
  }, []);
  useEffect(() => {
    themeConfig();
  }, [theme]);

  const themeConfig = () => {
    const key = constans.asyncStorage.THEME;
    util.as.add(key, theme);
  };

  function toggleTheme() {
    setTheme((theme) => (theme === 'light' ? 'dark' : 'light'));
  }

  const toggleRTL = React.useCallback(() => {
    I18nManager.forceRTL(!rtl);
    RNRestart.Restart();
  }, [rtl]);

  const preferences = React.useMemo(
    () => ({
      toggleTheme,
      toggleRTL,
      theme,
      rtl: rtl ? 'right' : 'left',
    }),
    [rtl, theme, toggleRTL],
  );
  console.log({Main: theme});

  return (
    <PreferencesContext.Provider value={preferences}>
      <PaperProvider
        theme={
          theme === 'light'
            ? {...DefaultTheme, ...light}
            : {...DarkTheme, ...dark}
        }>
        <RootNavigator />
      </PaperProvider>
    </PreferencesContext.Provider>
  );
};
