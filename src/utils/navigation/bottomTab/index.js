import React from 'react';
import color from 'color';
import {createMaterialBottomTabNavigator} from '@react-navigation/material-bottom-tabs';
import {useTheme, Portal, FAB} from 'react-native-paper';
import {useSafeArea} from 'react-native-safe-area-context';
import {useIsFocused, RouteProp} from '@react-navigation/native';
import CustomIcon from 'Rimtal/src//utils/CustomIcon';
import overlay from './overlay';
import home from './../../../screens/home';
import {DrawerContent} from '../drawerContent';
import discover from '../../../screens/discover';
import myMusic from '../../../screens/myMusic';
import profile from '../../../screens/profile';
// import {StackNavigatorParamlist} from './types';
import MusicPlayerSkin from '../../../baseComponents/MusicPlayer/MusicPlayerSkin';

const Tab = createMaterialBottomTabNavigator();

export const BottomTabs = (props) => {
  const routeName = props.route.state
    ? props.route.state.routes[props.route.state.index].name
    : 'Feed';

  const theme = useTheme();
  console.log({buton: theme});

  const safeArea = useSafeArea();
  const isFocused = useIsFocused();
  console.log({isFocused, safeArea, theme, props});

  let icon = 'feather';

  switch (routeName) {
    case 'Messages':
      icon = 'email-plus-outline';
      break;
    default:
      icon = 'feather';
      break;
  }

  const tabBarColor = theme.dark
    ? overlay(6, theme.colors.surface)
    : theme.colors.surface;

  return (
    <React.Fragment>
      <Tab.Navigator
        barStyle={{backgroundColor: 'white'}}
        shifting={false}
        labeled={true}
        initialRouteName="Home"
        backBehavior="initialRoute"
        activeColor={theme.customColors.primary}
        inactiveColor={'#888888'}
        sceneAnimationEnabled={true}>
        <Tab.Screen
          name="Home"
          component={home}
          options={{
            tabBarIcon: (color) => (
              <CustomIcon color={color.color} name="home" size={24} />
            ),
            tabBarLabel: 'Home',
            tabBarColor,
          }}
        />

<Tab.Screen
          name="Search"
          component={DrawerContent}
          options={{
            tabBarIcon: (color) => (
              <CustomIcon name="search" size={24} color={color.color} />
            ),
            tabBarColor,
          }}
        />

        <Tab.Screen
          name="Discover"
          component={discover}
          options={{
            tabBarIcon: (color) => (
              <CustomIcon name="compass" size={24} color={color.color} />
            ),
            tabBarColor,
          }}
        />

        <Tab.Screen
          name="MyMusic"
          component={myMusic}
          options={{
            tabBarIcon: (color) => (
              <CustomIcon name="music" size={24} color={color.color} />
            ),
            tabBarColor,
          }}
        />

        <Tab.Screen
          name="Profile"
          component={profile}
          options={{
            tabBarIcon: (color) => (
              <CustomIcon name="user" size={24} color={color.color} />
            ),
            tabBarColor,
          }}
        />

        {/* <Tab.Screen
          name="Feed"
          component={Feed}
          options={{
            tabBarIcon: 'home-account',
            tabBarColor,
          }}
        /> */}

        {/* <Tab.Screen
          name="Notifications"
          component={Notifications}
          options={{
            tabBarIcon: 'bell-outline',
            tabBarColor,
          }}
        /> */}
        {/* <Tab.Screen
          name="Messages"
          component={Message}
          options={{
            tabBarIcon: 'message-text-outline',
            tabBarColor,
          }}
        /> */}
     
        {/* <Tab.Screen
          name="Music"
          component={MusicPlayerSkin}
          options={{
            tabBarIcon: 'music',
            tabBarColor,
          }}
        /> */}
      </Tab.Navigator>
      {/* <Portal>
        <FAB
          visible={isFocused}
          icon={icon}
          style={{
            position: 'absolute',
            bottom: safeArea.bottom + 65,
            right: 16,
          }}
          color="white"
          theme={{
            colors: {
              accent: theme.colors.primary,
            },
          }}
          onPress={() => {}}
        />
      </Portal> */}
    </React.Fragment>
  );
};
