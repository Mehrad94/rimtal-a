import React from 'react';
import {FlatList, View, StyleSheet} from 'react-native';
// import {StackNavigationProp} from '@react-navigation/stack';
import {useTheme} from 'react-native-paper';
import {useFocusEffect} from '@react-navigation/native';

import {Twitt} from '../../components/twitt';
import {twitts} from './data';
function renderItem({item}) {
  return <Twitt {...item} />;
}

function keyExtractor(item) {
  return item.id.toString();
}

export const Feed = (props) => {
  useFocusEffect(
    React.useCallback(() => {
      // Do something when the screen is focused
      console.log('omad');

      return () => {
        console.log('raft');

        // Do something when the screen is unfocused
        // Useful for cleanup functions
      };
    }, []),
  );
  const theme = useTheme();

  const data = twitts.map((twittProps) => ({
    ...twittProps,
    onPress: () =>
      props.navigation &&
      props.navigation.push('Details', {
        ...twittProps,
      }),
  }));

  return (
    <FlatList
      contentContainerStyle={{backgroundColor: theme.colors.background}}
      style={{backgroundColor: theme.colors.background}}
      data={data}
      renderItem={renderItem}
      keyExtractor={keyExtractor}
      ItemSeparatorComponent={() => (
        <View style={{height: StyleSheet.hairineWidth}} />
      )}
    />
  );
};
