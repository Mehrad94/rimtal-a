import React from 'react';

export const PreferencesContext = React.createContext({
  rtl: 'left',
  theme: 'light',
  toggleTheme: () => {
    console.log('toggleTheme');
  },
  toggleRTL: () => {
    console.log('togglertl');
  },
});
