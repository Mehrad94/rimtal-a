import AsyncStorage from '@react-native-community/async-storage';
export function add(key, value) {
  AsyncStorage.setItem(key, value);
}
export function remove(key) {
  AsyncStorage.removeItem(key);
}

const as = {
  add,
  remove,
};
export default as;
